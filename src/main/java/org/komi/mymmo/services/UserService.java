package org.komi.mymmo.services;

import org.komi.mymmo.entities.User;
import org.komi.mymmo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    public Iterable<User> getUsers(){
       return userRepository.findAll();
    }


    /**
     * Register user
     * @param user
     * @return
     */
    public User signUp(User user){
        User u = userRepository.getUserByUsername(user.getUsername());
        if(u!=null){
            return null;
        }
        return  userRepository.save(user);
    }

    public User editUser(User user){
return null;
    }

    public void logOut(){

    }

    public Boolean isAuthenticate(){
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.getUserByUsername(username);
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }
}
