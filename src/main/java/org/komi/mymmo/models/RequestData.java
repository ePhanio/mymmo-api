package org.komi.mymmo.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RequestData {
    private String password;
    private String username;
    private String deviceId;


}
