package org.komi.mymmo.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class Property {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;
    private String latitude;
    private String longitude;
    private Double price;
    private String description;
    @ManyToOne
    private TypeOfProperty typeOfProperty;
    @ManyToOne
    private User user;

}
