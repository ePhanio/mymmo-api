package org.komi.mymmo.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private String email;
    private String username;
    @JoinColumn(nullable = true)
    private String address;
    private String phoneNumber;
    @JoinColumn(nullable = true)
    private String latitude;
    @JoinColumn(nullable = true)
    private String longitude;
    private String ville;
    private String deviceId;
    private String password;
    @ManyToOne
    private Country country;


}
